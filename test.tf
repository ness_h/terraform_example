# Configure the VMware vSphere Provider
provider "vsphere" {
    vsphere_server = "${var.vsphere_vcenter}"
    user = "${var.vsphere_user}"
    password = "${var.vsphere_password}"
    allow_unverified_ssl = true
}

# Build machine
resource "vsphere_virtual_machine" "DEVWLTVIPWRK" {
    name   = "DEVWLTVIPWRK"
    vcpu   = 8
    memory = 16384
    domain = "${var.domain}"
    datacenter = "${var.vsphere_datacenter}"
    cluster = "${var.vsphere_cluster}"

    # Define the Networking settings for the VM
    network_interface {
        label = "${var.vsphere_network_label}"
        ipv4_address = ""
    }

    dns_servers = ["10.216.201.1", "10.216.201.2", "10.89.201.1"]

    # Define the Disks and resources. The first disk should include the template.
    disk {
        template = "${var.vsphere_template}"
        datastore = "${var.vsphere_datastore}"
        type ="thin"
    }

    # Define Time Zone
    time_zone = "America/New_York"

    #Get in domain
    windows_opt_config {
     domain = "${var.domain}"
     domain_user = "${var.domain_user}"
     domain_user_password = "${var.domain_password}"
     admin_password = "${var.root_password}"
   }
}
