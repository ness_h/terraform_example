# Variables
variable "vsphere_vcenter" {}
variable "vsphere_user" {}
variable "vsphere_password" {}
variable "vsphere_datacenter" {}
variable "vsphere_cluster" {}
variable "root_password" {}
variable "vsphere_network_label" {}
variable "vsphere_template" {}
variable "vsphere_datastore" {}
variable "domain_user" {}
variable "domain_password" {}
variable "domain" {}